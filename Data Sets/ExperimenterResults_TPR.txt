Tester:     weka.experiment.PairedCorrectedTTester -G 3,4,5 -D 1 -R 2 -S 0.05 -result-matrix "weka.experiment.ResultMatrixPlainText -mean-prec 2 -stddev-prec 2 -col-name-width 0 -row-name-width 25 -mean-width 2 -stddev-width 2 -sig-width 1 -count-width 5 -print-col-names -print-row-names -enum-col-names"
Analysing:  True_positive_rate
Datasets:   1
Resultsets: 7
Confidence: 0.05 (two tailed)
Sorted by:  -
Date:       2-10-18 11:55


Dataset                   (1) rules.Z | (2) rule (3) tree (4) tree (5) lazy (6) func (7) baye
---------------------------------------------------------------------------------------------
weka                      (10)   1.00 |   0.99 *   0.98 *   1.00 *   1.00 *   0.99 *   0.54 *
---------------------------------------------------------------------------------------------
                              (v/ /*) |  (0/0/1)  (0/0/1)  (0/0/1)  (0/0/1)  (0/0/1)  (0/0/1)


Key:
(1) rules.ZeroR '' 48055541465867954
(2) rules.OneR '-B 6' -3459427003147861443
(3) trees.J48 '-C 0.25 -M 2' -217733168393644444
(4) trees.RandomForest '-P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1' 1116839470751428698
(5) lazy.IBk '-K 5 -W 0 -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"' -3080186098777067172
(6) functions.SimpleLogistic '-I 0 -M 500 -H 50 -W 0.0' 7397710626304705059
(7) bayes.NaiveBayes '' 5995231201785697655

